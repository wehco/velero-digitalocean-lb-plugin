# Velero DigitalOcean LoadBalancer Plugin

DigitalOcean adds UUID annotations to Kubernetes LoadBalancer type Services
which will link the services to the Load Balancer that is provisioned for them.
This means that if another service is created with the same annotation, the two
services will "compete" for control of the same DigitalOcean Load Balancer.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: hello-kubernetes
  annotations:
    kubernetes.digitalocean.com/load-balancer-id: a47d725a-ddc9-44d2-a5b5-b9f7f72e54a0
spec:
  type: LoadBalancer
  ports:
  - port: 80
    targetPort: 8080
  selector:
    app: hello-kubernetes
```

This can be an issue if you intend to use Velero to maintain a staging or
development cluster which is created from a backup of production, as the other
clusters can cause the production Load Balancer to fail. In order to prevent
this, the annotation must be removed from the service.

Because DigitalOcean does not allow you to reserve IPs for Load Balancers, it is
important to maintain the same Load Balancer UUID annotation in production
clusters, to prevent new ones being provisioned and changing the IP. Therefore
it is not recommended this plugin be installed in the production cluster from
which backups are made, rather it should probably only be installed in
staging/dev clusters to which restores are performed. It will, however, remove
the UUID annotation for both Backup and Restore operations for any cluster it is
installed in, so it remains an option to be used in that way if desired.

### Sample Log Entry for Backup with Plugin

```sh
$ velero backup logs hello-k8s | grep "Removing DO LoadBalancer"
time="2019-09-26T20:54:53Z" level=info msg="Removing DO LoadBalancer UUID from hello-kubernetes" backup=velero/hello-k8s cmd=/plugins/do-lb-plugin logSource="backup.go:41" pluginName=do-lb-plugin
```


### Sample Log Entry for Restore with Plugin

```sh
$ velero restore logs hello-k8s-20190926155845 | grep "Removing DO LoadBalancer"
time="2019-09-26T20:58:47Z" level=info msg="Removing DO LoadBalancer UUID from hello-kubernetes" cmd=/plugins/do-lb-plugin logSource="restore.go:39" pluginName=do-lb-plugin restore=velero/hello-k8s-20190926155845
```

## Install

```sh
velero plugin add registry.gitlab.com/wehco/velero-digitalocean-lb-plugin:v1
```

## Uninstall

```sh
velero plugin remove registry.gitlab.com/wehco/velero-digitalocean-lb-plugin:v1
```
