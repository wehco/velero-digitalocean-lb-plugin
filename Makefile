PKG := wehco/velero-digitalocean-lb-plugin
VERSION ?= $(shell git describe --tags --always --dirty --match="v*" 2> /dev/null || cat $(CURDIR)/.version 2> /dev/null || echo v0)
IMAGE ?= registry.gitlab.com/${PKG}:${VERSION}
GOOS = $(shell go env GOOS)
GOARCH = $(shell go env GOARCH)
BUILD_DIR ?= build

build: build-dirs
	CGO_ENABLED=0 go build -installsuffix "static" -i -v -o "${BUILD_DIR}/bin/do-lb-plugin" .

build-container: build
	cp Dockerfile "${BUILD_DIR}/Dockerfile"
	docker build -t "${IMAGE}" -f "${BUILD_DIR}/Dockerfile" "${BUILD_DIR}"

push-container: build-container
	docker push "${IMAGE}"

build-dirs:
	@mkdir -p "${BUILD_DIR}/bin"

clean:
	rm -rf "${BUILD_DIR}"
