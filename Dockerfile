FROM debian:stretch-slim
RUN mkdir /plugins
ADD bin/* /plugins/
USER nobody:nobody
ENTRYPOINT ["/bin/bash", "-c", "cp /plugins/* /target/."]
