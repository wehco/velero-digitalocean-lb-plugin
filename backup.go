package main

import (
	"github.com/sirupsen/logrus"

	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/runtime"

	v1 "github.com/heptio/velero/pkg/apis/velero/v1"
	"github.com/heptio/velero/pkg/plugin/velero"
)

// BackupPlugin is a backup item action plugin for Velero
type BackupPlugin struct {
	log logrus.FieldLogger
}

// AppliesTo returns information about which resources this action should be invoked for.
// A BackupPlugin's Execute function will only be invoked on items that match the returned
// selector. A zero-valued ResourceSelector matches all resources.
func (p *BackupPlugin) AppliesTo() (velero.ResourceSelector, error) {
	return velero.ResourceSelector{
		IncludedResources: []string{"services"},
	}, nil
}

// Execute allows the ItemAction to perform arbitrary logic with the item being backed up,
// in this case, setting a custom annotation on the item being backed up.
func (p *BackupPlugin) Execute(item runtime.Unstructured, backup *v1.Backup) (runtime.Unstructured, []velero.ResourceIdentifier, error) {
	object, err := meta.Accessor(item)
	if err != nil {
		return nil, nil, err
	}

	annotations := object.GetAnnotations()
	if annotations == nil {
		annotations = make(map[string]string)
	}

	if _, ok := annotations[Annotation]; ok {
		p.log.Infof("Removing DO LoadBalancer UUID from %s", object.GetName())
		delete(annotations, Annotation)
		object.SetAnnotations(annotations)
	}

	return item, nil, nil
}
