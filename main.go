package main

import (
	veleroplugin "github.com/heptio/velero/pkg/plugin/framework"
	"github.com/sirupsen/logrus"
)

const Annotation string = "kubernetes.digitalocean.com/load-balancer-id"

func main() {
	veleroplugin.NewServer().
		RegisterBackupItemAction("wehco.com/digitalocean-lb-backup-plugin", newBackupPlugin).
		RegisterRestoreItemAction("wehco.com/digitalocean-lb-restore-plugin", newRestorePlugin).
		Serve()
}

func newBackupPlugin(logger logrus.FieldLogger) (interface{}, error) {
	return &BackupPlugin{log: logger}, nil
}

func newRestorePlugin(logger logrus.FieldLogger) (interface{}, error) {
	return &RestorePlugin{log: logger}, nil
}
