module gitlab.org/wehco/infrastructure/velero/do-lb-plugin

go 1.13

require (
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/hashicorp/go-plugin v1.0.1 // indirect
	github.com/heptio/velero v1.1.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/net v0.0.0-20190923162816-aa69164e4478 // indirect
	google.golang.org/grpc v1.23.1 // indirect
	k8s.io/api v0.0.0-20190923155552-eac758366a00 // indirect
	k8s.io/apimachinery v0.0.0-20190923155427-ec87dd743e08
)
