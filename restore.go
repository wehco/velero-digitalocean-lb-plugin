package main

import (
	"github.com/sirupsen/logrus"

	"k8s.io/apimachinery/pkg/api/meta"

	"github.com/heptio/velero/pkg/plugin/velero"
)

// RestorePlugin is a backup item action plugin for Velero
type RestorePlugin struct {
	log logrus.FieldLogger
}

// AppliesTo returns information about which resources this action should be invoked for.
// A RestorePlugin's Execute function will only be invoked on items that match the returned
// selector. A zero-valued ResourceSelector matches all resources.
func (p *RestorePlugin) AppliesTo() (velero.ResourceSelector, error) {
	return velero.ResourceSelector{
		IncludedResources: []string{"services"},
	}, nil
}

// Execute allows the ItemAction to perform arbitrary logic with the item being backed up,
// in this case, setting a custom annotation on the item being backed up.
func (p *RestorePlugin) Execute(input *velero.RestoreItemActionExecuteInput) (*velero.RestoreItemActionExecuteOutput, error) {
	object, err := meta.Accessor(input.Item)
	if err != nil {
		return &velero.RestoreItemActionExecuteOutput{}, err
	}

	annotations := object.GetAnnotations()
	if annotations == nil {
		annotations = make(map[string]string)
	}

	if _, ok := annotations[Annotation]; ok {
		p.log.Infof("Removing DO LoadBalancer UUID from %s", object.GetName())
		delete(annotations, Annotation)
		object.SetAnnotations(annotations)
	}

	return velero.NewRestoreItemActionExecuteOutput(input.Item), nil
}
